//file:DownLoadClient.java   
package org.piaozhiye.study;  
import java.io.IOException;  
import java.net.Socket;  
import android.app.Activity;  
import android.os.Bundle;  
import android.view.View;  
import android.widget.Button;  
import android.widget.EditText;  
public class DownLoadClient extends Activity {  
    private Button download = null;  
    private EditText et_serverIP = null;  
    private EditText et_fileName= null;  
    private String downloadFile = null;  
    private final static int PORT = 65525;  
    private final static String defaultIP = "192.168.0.100";  
    private static String serverIP = null;  
    private Socket socket;  
    /** Called when the activity is first created. */  
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.main);  
        download = (Button)findViewById(R.id.download);  
        et_serverIP= (EditText)findViewById(R.id.et_serverip);  
        et_fileName = (EditText)findViewById(R.id.et_filename);  
        et_serverIP.setText("192.168.0.100");  
        
      download.setOnClickListener(new View.OnClickListener() {  
              
            @Override  
            public void onClick(View v) {  
                 serverIP = et_serverIP.getText().toString();  
                 if(serverIP == null){  
                     serverIP = defaultIP;  
                 }  
                 System.out.println("DownLoadClient serverIP--->" + serverIP );  
                    System.out.println("DownLoadClient MainThread--->" + Thread.currentThread().getId() );  
                      
                try{  
                    socket = new Socket(serverIP, PORT);  
                      
                }catch(IOException e){  
                      
                }  
                 downloadFile = et_fileName.getText().toString();  
            Thread downFileThread = new Thread(new DownFileThread(socket, downloadFile));  
            downFileThread.start();  
                System.out.println("DownLoadClient downloadFile--->" + downloadFile );  
            }  
        });  
         
    }  
}  